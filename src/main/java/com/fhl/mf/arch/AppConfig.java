package com.fhl.mf.arch;

public class AppConfig {

	private String uiActionName;
	private String actionClass;
	private String successPage;
	private String errorPage;
	
	/**
	 * @return the uiActionName
	 */
	public String getUiActionName() {
		return uiActionName;
	}
	/**
	 * @param uiActionName the uiActionName to set
	 */
	public void setUiActionName(String uiActionName) {
		this.uiActionName = uiActionName;
	}
	/**
	 * @return the actionClass
	 */
	public String getActionClass() {
		return actionClass;
	}
	/**
	 * @param actionClass the actionClass to set
	 */
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	/**
	 * @return the successPage
	 */
	public String getSuccessPage() {
		return successPage;
	}
	/**
	 * @param successPage the successPage to set
	 */
	public void setSuccessPage(String successPage) {
		this.successPage = successPage;
	}
	/**
	 * @return the errorPage
	 */
	public String getErrorPage() {
		return errorPage;
	}
	/**
	 * @param errorPage the errorPage to set
	 */
	public void setErrorPage(String errorPage) {
		this.errorPage = errorPage;
	}
}
