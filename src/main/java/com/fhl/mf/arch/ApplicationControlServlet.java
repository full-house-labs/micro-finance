package com.fhl.mf.arch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.fhl.mf.utils.FileUtils;
import com.fhl.mf.utils.MFApplicationConstants;

/**
 * Servlet implementation
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, maxFileSize = 1024 * 1024 * 10, maxRequestSize = 1024 * 1024 * 50)
public class ApplicationControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Map<String, AppConfig> appConfigMap = new HashMap<String, AppConfig>();

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			InputStream appConfig = config.getServletContext()
					.getResourceAsStream(MFApplicationConstants.APP_CONFIG_RESOURCE_PATH);
			FileUtils.buildAppConfigMap(appConfig, appConfigMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Default constructor.
	 */
	public ApplicationControlServlet() {
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//super.service(request, response);
		String uiActionName = request.getParameter("uiActionName");
		AppConfig appConfig = appConfigMap.get(uiActionName);
		try {
			Class<?> actionClass = Class.forName(appConfig.getActionClass());
			Action actionIf = (Action) actionClass.newInstance();
			String successPage = actionIf.execute(request, response, appConfig);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(successPage);
			dispatcher.forward(request, response);
		} catch (ClassNotFoundException | InstantiationException |
				IllegalAccessException e) {
			request.setAttribute("errorMessage", e.getStackTrace());
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(appConfig.getErrorPage());
			dispatcher.forward(request, response);
		}
		
	}
	
}
