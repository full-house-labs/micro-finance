package com.fhl.mf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fhl.mf.model.Customer;
import com.fhl.mf.utils.DBUtils;

public class CustomerServiceDao {

	public List<Customer> getCustomers() {
		Connection con = null;
		ResultSet rs = null;
		Customer customer = null;
		List<Customer> customers = new ArrayList<Customer>();
		try {
			con = DBUtils.getConnection();
			String query = "select * from fhl_mf.fhl_mf_customers;";
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				customer = new Customer();
				customer.setFirstName(rs.getString("mf_customers_fname"));
				customer.setLastName(rs.getString("mf_customers_lname"));
				customer.setHomePhone(rs.getString("mf_customers_phone_primary"));
				customer.setHomePhoneCountryCode(rs.getString("mf_customers_phone_primary_countrycode"));
				customer.setEmailId(rs.getString("mf_customers_email"));
				customers.add(customer);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return customers;
	}
	
	public String createCustomer(HttpServletRequest request) {
		
		Connection con = null;
		ResultSet rs = null;
		int i=0;
		String firstName = request.getParameter("exampleFirstName");
		String lastName = request.getParameter("exampleLastName");
		String contactNumber = request.getParameter("exampleContactNumber");
		String contactCountryCode = request.getParameter("exampleContactCountryCode");	
		String emailId = request.getParameter("exampleInputEmail");
		//System.out.println(firstName);

		//System.out.println(emailId);
		try {
			con = DBUtils.getConnection();
			String query = "insert into fhl_mf.fhl_mf_customers (mf_customers_fname,mf_customers_lname,mf_customers_phone_primary,mf_customers_phone_primary_countrycode,mf_customers_email) values(?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(query);
			
			stmt.setString(1, firstName);			
			stmt.setString(2, lastName);
			stmt.setString(3, contactNumber);
			stmt.setString(4, contactCountryCode);
			stmt.setString(5, emailId);
			
			//rs = stmt.executeQuery(query);
			i=stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(i==1)
			return "Customer created successfully";
		else
			return null;
	}

}
