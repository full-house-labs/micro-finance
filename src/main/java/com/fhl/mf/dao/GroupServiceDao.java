package com.fhl.mf.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.fhl.mf.model.Group;
import com.fhl.mf.utils.DBUtils;

public class GroupServiceDao {


	public List<Group> getGroups() {
		Connection con = null;
		ResultSet rs = null;
		Group group = null;
		List<Group> groups = new ArrayList<Group>();
		try {
			con = DBUtils.getConnection();
			String query = "select * from fhl_mf.fhl_mf_groups;";
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				group = new Group();
				group.setGroupName(rs.getString("mf_group_name"));
				group.setGroupDescription(rs.getString("mf_group_description"));
				group.setGroupIsActive(rs.getString("mf_group_isactive").charAt(0));
				//System.out.println(rs.getCharacterStream("mf_group_isactive").toString());
				//System.out.println(group.getGroupIsActive());
				group.setGroupCreatedDate(rs.getDate("mf_group_created_date"));
				groups.add(group);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return groups;
	}

}
