package com.fhl.mf.model;

public class Customer {

	private String firstName;
	private String lastName;
	private String homePhone;
	private String homePhoneCountryCode;
	private String emailId;
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return homePhone;
	}
	/**
	 * @param homePhone the homePhone to set
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	/**
	 * @return the homePhoneCountryCode
	 */
	public String getHomePhoneCountryCode() {
		return homePhoneCountryCode;
	}
	/**
	 * @param homePhoneCountryCode the homePhoneCountryCode to set
	 */
	public void setHomePhoneCountryCode(String homePhoneCountryCode) {
		this.homePhoneCountryCode = homePhoneCountryCode;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	
}
