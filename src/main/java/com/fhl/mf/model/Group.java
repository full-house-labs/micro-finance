package com.fhl.mf.model;

import java.util.Date;

public class Group {

	private String groupName;
	private String groupDescription;
	private char groupIsActive;
	private Date groupCreatedDate;
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the groupDescription
	 */
	public String getGroupDescription() {
		return groupDescription;
	}
	/**
	 * @param groupDescription the groupDescription to set
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	
	/**
	 * @return the groupCreatedDate
	 */
	public Date getGroupCreatedDate() {
		return groupCreatedDate;
	}
	/**
	 * @param groupCreatedDate the groupCreatedDate to set
	 */
	public void setGroupCreatedDate(Date groupCreatedDate) {
		this.groupCreatedDate = groupCreatedDate;
	}
	/**
	 * @return the groupIsActive
	 */
	public char getGroupIsActive() {
		return groupIsActive;
	}
	/**
	 * @param groupIsActive the groupIsActive to set
	 */
	public void setGroupIsActive(char groupIsActive) {
		this.groupIsActive = groupIsActive;
	}
	
	
}
