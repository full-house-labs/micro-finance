package com.fhl.mf.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fhl.mf.arch.Action;
import com.fhl.mf.arch.AppConfig;
import com.fhl.mf.dao.CustomerServiceDao;
import com.fhl.mf.model.Customer;

public class CreateCustomerService implements Action {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, AppConfig appConfig) {
		CustomerServiceDao dao = new CustomerServiceDao();
		//System.out.println("aaaaaaaa");
		String message=dao.createCustomer(request);
		//List<Customer> customers = dao.getCustomers();
		request.setAttribute("SuccessMessage", message);
		return appConfig.getSuccessPage();
	}

}