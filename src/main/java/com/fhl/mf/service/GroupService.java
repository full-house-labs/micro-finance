package com.fhl.mf.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fhl.mf.arch.Action;
import com.fhl.mf.arch.AppConfig;
import com.fhl.mf.dao.GroupServiceDao;
import com.fhl.mf.model.Group;

public class GroupService implements Action{


	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, AppConfig appConfig) {
		GroupServiceDao dao = new GroupServiceDao();
		List<Group> groups = dao.getGroups();
		request.setAttribute("groups", groups);
		return appConfig.getSuccessPage();
	}
}
