package com.fhl.mf.utils;

import java.io.InputStream;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.fhl.mf.arch.AppConfig;

public class FileUtils {

	public static void buildAppConfigMap(InputStream appConfigFile, Map<String, AppConfig> appConfigMap) {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

		xmlInputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		xmlInputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(appConfigFile);
			AppConfig appConfig = null;
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
					case "action":
						appConfig = new AppConfig();
						break;
					case "name":
						nextEvent = reader.nextEvent();
						appConfig.setUiActionName(nextEvent.asCharacters().getData());
						break;
					case "class":
						nextEvent = reader.nextEvent();
						appConfig.setActionClass(nextEvent.asCharacters().getData());
						break;
					case "successPage":
						nextEvent = reader.nextEvent();
						appConfig.setSuccessPage(nextEvent.asCharacters().getData());
						break;
					case "errorPage":
						nextEvent = reader.nextEvent();
						appConfig.setErrorPage(nextEvent.asCharacters().getData());
						break;
					}
				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("action")) {
						appConfigMap.put(appConfig.getUiActionName(), appConfig);
					}
				}
			}

		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

}
