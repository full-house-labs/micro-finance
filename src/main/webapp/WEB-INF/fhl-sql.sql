CREATE DATABASE `fhl_mf` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
CREATE TABLE `fhl_mf_adminusers` (
  `mf_admin_username` varchar(45) NOT NULL,
  `mf_admin_password` varchar(45) NOT NULL,
  PRIMARY KEY (`mf_admin_password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
CREATE TABLE `fhl_mf_customers` (
  `mf_customers_fname` varchar(45) DEFAULT NULL,
  `mf_customers_lname` varchar(45) DEFAULT NULL,
  `mf_customers_phone_primary` varchar(45) DEFAULT NULL,
  `mf_customers_phone_primary_countrycode` varchar(45) DEFAULT NULL,
  `mf_customers_email` varchar(45) NOT NULL,
  `mf_customers_id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`mf_customers_id`),
  UNIQUE KEY `mf_customers_email_UNIQUE` (`mf_customers_email`),
  UNIQUE KEY `mf_customers_id_UNIQUE` (`mf_customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
CREATE TABLE `fhl_mf_groups` (
  `mf_group_name` varchar(45) NOT NULL,
  `mf_group_description` varchar(200) DEFAULT NULL,
  `mf_group_isactive` char(1) NOT NULL,
  `mf_group_created_date` datetime NOT NULL,
  PRIMARY KEY (`mf_group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Group table in microfinance project';
